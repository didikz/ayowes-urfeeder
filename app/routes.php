<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', ['as'=>'landing', 'uses'=>'HomeController@index']);
Route::get('login', ['as'=>'loginPage', 'uses' => 'HomeController@getLoginPage']);
Route::post('login', ['as'=>'postLogin', 'uses' => 'HomeController@postLogin']);
Route::get('logout', ['as'=>'logout', 'uses'=>'HomeController@logout']);
Route::get('admin/dashboard', ['as'=>'dashboard', 'before'=>'auth', 'uses'=> 'DashboardController@index']);
Route::get('admin/feeder', ['as'=>'feederCollection', 'before'=>'auth', 'uses'=>'DashboardController@getFeeder']);
Route::get('admin/feeder/create', ['as'=>'createFeeder', 'before'=>'auth', 'uses'=>'DashboardController@createFeeder']);