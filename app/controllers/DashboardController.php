<?php

class DashboardController extends BaseController {

	public function index()
	{
		return View::make('dashboardindex');
	}

	public function getFeeder()
	{
		$data = DB::table('feeder_collection')->where('user_id','=',Auth::id())->get();

		return View::make('collection', ['feeders'=>$data]);
	}
}
