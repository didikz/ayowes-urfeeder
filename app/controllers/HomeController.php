<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		return View::make('landing');
	}

	public function getLoginPage()
	{
		return View::make('login');
	}

	public function postLogin()
	{
		if(Auth::attempt(array('email'=>Input::get('email'),'password'=>Input::get('password')))) {
			return Redirect::intended('admin/dashboard');
		} else {
			return Redirect::route('loginPage');
		}
	}

	public function logout()
	{
		Session::flush();
		return Redirect::route('landing');
	}

}
