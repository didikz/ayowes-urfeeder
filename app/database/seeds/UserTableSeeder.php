<?php

class UserTableSeeder extends Seeder {

public function run() {
	DB::table('users')->truncate();

	DB::table('users')->insert(array(
				'email' => 'admin@urfeed.com',
				'password' => Hash::make('admin'),
				'name' => 'admin',
				'level' => '0'
		));	

	DB::table('users')->insert(array(
				'email' => 'rezd14@gmail.com',
				'password' => Hash::make('123456'),
				'name' => 'Didik Tri Susanto',
				'level' => '1'
		));	
}
}

