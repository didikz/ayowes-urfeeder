<?php

class CollectionTableSeeder extends Seeder {

public function run() {

	DB::table('feeder_collection')->truncate();

	DB::table('feeder_collection')->insert(array(
				'user_id' => 2,
				'feeder_name' => 'teknomuslim',
				'url_feeder' => 'http://teknomuslim.com/feed/',
				'published' => '1'
		));	
}
}

