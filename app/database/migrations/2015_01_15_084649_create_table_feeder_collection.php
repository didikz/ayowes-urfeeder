<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFeederCollection extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feeder_collection', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id', false);
			$table->string('feeder_name');
			$table->string('url_feeder');
			$table->enum('published',['0','1'])->default('0');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feeder_collection');
	}

}
