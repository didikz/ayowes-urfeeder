@extends('layouts.dashboard')
@section('content')
<h2 class="sub-header">RSS Collection</h2>
  <div class="table-responsive">
    <table class="table table-striped">
    	<thead>
    		<th>No.</th>
    		<th>Name</th>
    		<th>Source</th>
    		<th>Status</th>
    		<th>Action</th>
    	</thead>
    	<tbody>
    	<?php $no=1; ?>
    	@foreach($feeders as $feeder)
    		<tr>
    			<td>{{$no}}</td>
    			<td>{{ $feeder->feeder_name }}</td>
    			<td>{{ $feeder->url_feeder }}</td>
    			<td>
    			@if($feeder->published == '0')
    				unpublished
    			@else
    				published
    			@endif
    			</td>
    			<td>Edit | Hapus</td>
    		</tr>
    	<?php $no++; ?>	
    	@endforeach
    	</tbody>
    </table>
   </div> 	
@stop